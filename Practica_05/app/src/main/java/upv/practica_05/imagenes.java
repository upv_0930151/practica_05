package upv.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class imagenes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagenes);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        String fruta = intent.getStringExtra(MainActivity.fruta);
        ImageView imagen =(ImageView) findViewById(R.id.imagen_imagen);
        switch (fruta){
            case "manzana":
                imagen.setImageResource(R.mipmap.manzana);
            case "pinia":
                imagen.setImageResource(R.mipmap.pinia);
            case "uva":
                imagen.setImageResource(R.mipmap.uvas);
            case "sandia":
                imagen.setImageResource(R.mipmap.sandia);
        }
    }

}
