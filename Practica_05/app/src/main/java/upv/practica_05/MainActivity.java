package upv.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public final static String fruta = "upv.Practica_05.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void enviarFruta(View view){
        Intent intent = new Intent(this,imagenes.class);
        switch (view.getId()){
            case R.id.btn_manzana:
                intent.putExtra(fruta,"manzana");
            case R.id.btn_pinia:
                intent.putExtra(fruta,"pinia");
            case R.id.btn_sandia:
                intent.putExtra(fruta,"sandia");
            case R.id.btn_uva:
                intent.putExtra(fruta,"uva");
        }
        startActivity(intent);
    }

}
